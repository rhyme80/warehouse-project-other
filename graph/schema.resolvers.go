package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"math/rand"
	"warehouse-project/controller"
	"warehouse-project/graph/generated"
	"warehouse-project/graph/model"
)

func (r *mutationResolver) AddRegister(ctx context.Context, inputRegister model.InputRegister) (*model.Register, error) {
	return controller.AddRegister(inputRegister)
}

func (r *mutationResolver) UpdateRegister(ctx context.Context, updateRegister *model.InputUpdateRegister) (*model.Register, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) RemoveRegister(ctx context.Context, idRegister string) (bool, error) {
	return controller.RemoveRegister(idRegister)
}

func (r *mutationResolver) AddArticle(ctx context.Context, inputArticle model.InputArticle) (*model.Article, error) {
	return controller.AddArticle(inputArticle)
}

func (r *mutationResolver) UpdateArticle(ctx context.Context, updateArticle *model.InputUpdateArticle) (*model.Article, error) {
	return controller.UpdateArticle(updateArticle.ID, *updateArticle.ArticleUpdate)
}

func (r *mutationResolver) RemoveArticle(ctx context.Context, idArticle string) (bool, error) {
	return controller.RemoveArticle(idArticle)
}

func (r *mutationResolver) AddCategory(ctx context.Context, inputCategory model.InputCategory) (*model.Category, error) {
	categoryNew, _ := controller.AddCategory(inputCategory)
	return categoryNew, nil
}

func (r *mutationResolver) UpdateCategory(ctx context.Context, updateCategory model.InputUpdateCategory) (*model.Category, error) {
	category, _ := controller.UpdateCategory(updateCategory.ID, *updateCategory.CategoryUpdate)
	return category, nil
}

func (r *mutationResolver) RemoveCategory(ctx context.Context, idCategory string) (bool, error) {
	return controller.RemoveCategory(idCategory)
}

func (r *mutationResolver) AddWarehouse(ctx context.Context, inputWarehouse model.InputWarehouse) (*model.Warehouse, error) {
	warehouseNew, _ := controller.AddWarehouse(inputWarehouse)
	return warehouseNew, nil
}

func (r *mutationResolver) UpdateWarehouse(ctx context.Context, updateWarehouse *model.InputUpdateWarehouse) (*model.Warehouse, error) {
	warehouse, _ := controller.UpdateWarehouse(updateWarehouse.ID, *updateWarehouse.WarehouseUpdate)
	return warehouse, nil
}

func (r *mutationResolver) RemoveWarehouse(ctx context.Context, idWarehouse string) (bool, error) {
	return controller.RemoveWarehouse(idWarehouse)
}

func (r *mutationResolver) AddUnitMeasure(ctx context.Context, inputUnitMeasure model.InputUnitMeasure) (*model.UnitMeasure, error) {
	unitMeasureNew, _ := controller.AddUnitMeasure(inputUnitMeasure)
	return unitMeasureNew, nil
}

func (r *mutationResolver) UpdateUnitMeasure(ctx context.Context, updateUnitMeasure *model.InputUpdateUnitMeasure) (*model.UnitMeasure, error) {
	unitMeasure, _ := controller.UpdateUnitMeasure(updateUnitMeasure.ID, *updateUnitMeasure.UnitMeasureUpdate)
	return unitMeasure, nil
}

func (r *mutationResolver) RemoveUnitMeasure(ctx context.Context, idUnitMeasure string) (bool, error) {
	return controller.RemoveUnitMeasure(idUnitMeasure)
}

func (r *mutationResolver) CreateBook(ctx context.Context, input model.NewBook) (*model.Book, error) {
	// pasar toda este bloque a la funcion save
	book := &model.Book{
		ID:    fmt.Sprintf("T%d", rand.Int()),
		Title: input.Title,
		Author: &model.User{
			ID:   input.UserID,
			Name: input.Name,
		},
	}
	controller.Save(book)
	return book, nil
}

func (r *mutationResolver) CreateProduct(ctx context.Context, input model.NewProduct) (*model.Book, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) GetRegister(ctx context.Context, idRegister string) (*model.Register, error) {
	return controller.GetRegister(idRegister)
}

func (r *queryResolver) GetRegisters(ctx context.Context) ([]*model.Register, error) {
	return controller.GetRegisters()
}

func (r *queryResolver) GetArticle(ctx context.Context, idArticle string) (*model.Article, error) {
	return controller.GetArticle(idArticle)
}

func (r *queryResolver) GetArticles(ctx context.Context) ([]*model.Article, error) {
	return controller.GetArticles()
}

func (r *queryResolver) GetWarehouse(ctx context.Context, idWarehouse string) (*model.Warehouse, error) {
	return controller.GetWarehouse(idWarehouse)
}

func (r *queryResolver) GetWarehouses(ctx context.Context) ([]*model.Warehouse, error) {
	return controller.GetWarehouses()
}

func (r *queryResolver) GetUnitMeasure(ctx context.Context, idUnitMeasure string) (*model.UnitMeasure, error) {
	return controller.GetUnitMeasure(idUnitMeasure)
}

func (r *queryResolver) GetUnitMeasures(ctx context.Context) ([]*model.UnitMeasure, error) {
	return controller.GetUnitMeasures()
}

func (r *queryResolver) GetCategory(ctx context.Context, idCategory string) (*model.Category, error) {
	return controller.GetCategory(idCategory)
}

func (r *queryResolver) GetCategories(ctx context.Context) ([]*model.Category, error) {
	return controller.GetCategories()
}

func (r *queryResolver) Books(ctx context.Context) ([]*model.Book, error) {
	books := controller.FindAll()
	return books, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
