package controller

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"warehouse-project/database"
	"warehouse-project/graph/model"
)

const (
	CollectionWarehouse = "warehouses"
)

type ControllerWarehouse interface {
	Add(book *model.Warehouse) (*model.Warehouse, error)
	Update(id string, book *model.Warehouse) *model.Warehouse
	Remove(id string) bool
	GetById(id string) *model.Warehouse
	GetAll() []*model.Warehouse
}

func AddWarehouse(warehouseInput model.InputWarehouse) (*model.Warehouse, error) {
	c, errC := database.GetCollection(CollectionWarehouse)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	idWarehouse := primitive.NewObjectID()
	warehouse := bson.M{
		"id":          idWarehouse,
		"name":        warehouseInput.Name,
		"description": warehouseInput.Description,
	}
	_, errC = c.InsertOne(database.Ctx, warehouse)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	return GetWarehouse(idWarehouse.Hex())
}

func GetWarehouse(idWarehouse string) (*model.Warehouse, error) {
	c, errC := database.GetCollection(CollectionWarehouse)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdWarehouse, _ := primitive.ObjectIDFromHex(idWarehouse)
	var result model.Warehouse
	errC = c.FindOne(database.Ctx, bson.M{"id": objectIdWarehouse}).Decode(&result)
	return &result, errC
}

func UpdateWarehouse(idWarehouse string, warehouse model.InputWarehouse) (*model.Warehouse, error) {
	c, errC := database.GetCollection(CollectionWarehouse)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdWarehouse, _ := primitive.ObjectIDFromHex(idWarehouse)
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"id": objectIdWarehouse},
		bson.M{"$set": warehouse},
	)
	if err != nil {
		return nil, fmt.Errorf("Error 500. Couldn't update warehouse %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return nil, errors.New("Error 404. Holiday not found ")
	}
	return GetWarehouse(idWarehouse)
}

func RemoveWarehouse(idWarehouse string) (bool, error) {
	c, errC := database.GetCollection(CollectionWarehouse)
	if errC != nil {
		return false, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdWarehouse, _ := primitive.ObjectIDFromHex(idWarehouse)
	result, err := c.DeleteOne(database.Ctx, bson.M{"id": objectIdWarehouse})
	if err != nil {
		return false, fmt.Errorf("Error 500. Couldn't delete warehouse %v ", err)
	}
	if result.DeletedCount == 0 {
		return false, errors.New("Error 404. Warehouse not found ")
	}
	return true, nil
}

func GetWarehouses() ([]*model.Warehouse, error) {
	c, errorC := database.GetCollection(CollectionWarehouse)
	if errorC != nil {
		return nil, errorC
	}
	var results []*model.Warehouse
	cursor, err := c.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}
	err = cursor.All(context.TODO(), &results)
	return results, err
}
