package controller

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"warehouse-project/database"
	"warehouse-project/graph/model"
)

const (
	CollectionUnitMeasure = "unitMeasures"
)

type ControllerUnitMeasure interface {
	Add(book *model.UnitMeasure) (*model.UnitMeasure, error)
	Update(id string, book *model.UnitMeasure) *model.UnitMeasure
	Remove(id string) bool
	GetById(id string) *model.UnitMeasure
	GetAll() []*model.UnitMeasure
}

func AddUnitMeasure(unitMeasureInput model.InputUnitMeasure) (*model.UnitMeasure, error) {
	c, errC := database.GetCollection(CollectionUnitMeasure)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	idUnitMeasure := primitive.NewObjectID()
	unitMeasure := bson.M{
		"id":           idUnitMeasure,
		"name":         unitMeasureInput.Name,
		"abbreviation": unitMeasureInput.Abbreviation,
	}
	_, errC = c.InsertOne(database.Ctx, unitMeasure)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	return GetUnitMeasure(idUnitMeasure.Hex())
}

func GetUnitMeasure(idUnitMeasure string) (*model.UnitMeasure, error) {
	c, errC := database.GetCollection(CollectionUnitMeasure)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdUnitMeasure, _ := primitive.ObjectIDFromHex(idUnitMeasure)
	var result model.UnitMeasure
	errC = c.FindOne(database.Ctx, bson.M{"id": objectIdUnitMeasure}).Decode(&result)
	return &result, errC
}

func UpdateUnitMeasure(idUnitMeasure string, unitMeasure model.InputUnitMeasure) (*model.UnitMeasure, error) {
	c, errC := database.GetCollection(CollectionUnitMeasure)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdUnitMeasure, _ := primitive.ObjectIDFromHex(idUnitMeasure)
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"id": objectIdUnitMeasure},
		bson.M{"$set": unitMeasure},
	)
	if err != nil {
		return nil, fmt.Errorf("Error 500. Couldn't update unitMeasure %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return nil, errors.New("Error 404. Holiday not found ")
	}
	return GetUnitMeasure(idUnitMeasure)
}

func RemoveUnitMeasure(idUnitMeasure string) (bool, error) {
	c, errC := database.GetCollection(CollectionUnitMeasure)
	if errC != nil {
		return false, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdUnitMeasure, _ := primitive.ObjectIDFromHex(idUnitMeasure)
	result, err := c.DeleteOne(database.Ctx, bson.M{"id": objectIdUnitMeasure})
	if err != nil {
		return false, fmt.Errorf("Error 500. Couldn't delete unitMeasure %v ", err)
	}
	if result.DeletedCount == 0 {
		return false, errors.New("Error 404. UnitMeasure not found ")
	}
	return true, nil
}

func GetUnitMeasures() ([]*model.UnitMeasure, error) {
	c, errorC := database.GetCollection(CollectionUnitMeasure)
	if errorC != nil {
		return nil, errorC
	}
	var results []*model.UnitMeasure
	cursor, err := c.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}
	err = cursor.All(context.TODO(), &results)
	return results, err
}
