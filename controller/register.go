package controller

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
	"warehouse-project/database"
	"warehouse-project/graph/model"
)

const (
	CollectionRegister = "registers"
)

type ControllerRegister interface {
	Add(book *model.Register) (*model.Register, error)
	Update(id string, book *model.Register) *model.Register
	Remove(id string) bool
	GetById(id string) *model.Register
	GetAll() []*model.Register
}

func AddRegister(registerInput model.InputRegister) (*model.Register, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	idRegister := primitive.NewObjectID()
	objectIDWarehouse, _ := primitive.ObjectIDFromHex(registerInput.IDWarehouse)
	objectIdArticle, _ := primitive.ObjectIDFromHex(registerInput.IDArticle)
	register := bson.M{
		"_id":         idRegister,
		"id":          idRegister,
		"idWarehouse": objectIDWarehouse,
		"idArticle":   objectIdArticle,
		"date":        time.Now(),
		"cant":        registerInput.Cant,
		"observation": registerInput.Observation,
		"type":        registerInput.Type,
	}
	_, errC = c.InsertOne(database.Ctx, register)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}

	// add to article
	_ = CalcInputsOutput(registerInput.IDArticle, registerInput.IDWarehouse)

	return GetRegister(idRegister.Hex())
}

func UpdateRegister(idRegister string, registerInput model.InputRegister) (*model.Register, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdRegister, _ := primitive.ObjectIDFromHex(idRegister)
	objectIDWarehouse, _ := primitive.ObjectIDFromHex(registerInput.IDWarehouse)
	objectIdArticle, _ := primitive.ObjectIDFromHex(registerInput.IDArticle)
	register := bson.M{
		"id":          idRegister,
		"idWarehouse": objectIDWarehouse,
		"idArticle":   objectIdArticle,
		"cant":        registerInput.Cant,
		"observation": registerInput.Observation,
		"type":        registerInput.Type,
	}
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"id": objectIdRegister},
		bson.M{"$set": register},
	)
	if err != nil {
		return nil, fmt.Errorf("Error 500. Couldn't update register %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return nil, errors.New("Error 404. Holiday not found ")
	}
	return GetRegister(idRegister)
}

func RemoveRegister(idRegister string) (bool, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return false, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdRegister, _ := primitive.ObjectIDFromHex(idRegister)
	result, err := c.DeleteOne(database.Ctx, bson.M{"id": objectIdRegister})
	if err != nil {
		return false, fmt.Errorf("Error 500. Couldn't delete register %v ", err)
	}
	if result.DeletedCount == 0 {
		return false, errors.New("Error 404. Register not found ")
	}
	register, _ := GetRegister(idRegister)
	_ = CalcInputsOutput(register.Article.ID, register.Warehouse.ID)
	return true, nil
}

func GetRegister(idRegister string) (*model.Register, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdRegister, _ := primitive.ObjectIDFromHex(idRegister)
	var result model.Register
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"_id": objectIdRegister,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "articles",
				"localField":   "idArticle",
				"foreignField": "id",
				"as":           "article",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$article",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "warehouses",
				"localField":   "idWarehouse",
				"foreignField": "id",
				"as":           "warehouse",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$warehouse",
				"preserveNullAndEmptyArrays": true,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	for cursor.Next(context.TODO()) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

func GetRegisters() ([]*model.Register, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	var results []*model.Register
	pipeline := []bson.M{
		{
			"$lookup": bson.M{
				"from":         "articles",
				"localField":   "idArticle",
				"foreignField": "id",
				"as":           "article",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$article",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "warehouses",
				"localField":   "idWarehouse",
				"foreignField": "id",
				"as":           "warehouse",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$warehouse",
				"preserveNullAndEmptyArrays": true,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	err = cursor.All(context.TODO(), &results)
	if err != nil {
		return nil, err
	}
	return results, nil
}

func CalcInputsOutput(idArticle, idWarehouse string) error {
	objectIDWarehouse, _ := primitive.ObjectIDFromHex(idWarehouse)
	objectIdArticle, _ := primitive.ObjectIDFromHex(idArticle)
	totalRegister, err := TotalCantMov(objectIdArticle, objectIDWarehouse)
	if err != nil {
		return nil
	}
	_ = UpdateStockArticle(objectIdArticle, totalRegister.TotalInput-totalRegister.TotalOutput)
	return nil
}

func TotalCantMov(idArticle, idWarehouse primitive.ObjectID) (*TotalRegisters, error) {
	fmt.Println("y0")
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, errC
	}
	var result TotalRegisters
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"$expr": bson.M{
					"$and": []bson.M{
						{
							"$eq": []interface{}{"$idArticle", idArticle},
						},
						{
							"$eq": []interface{}{"$idWarehouse", idWarehouse},
						},
					},
				},
			},
		},
		{
			"$group": bson.M{
				"_id": nil,
				"totalInput": bson.M{
					"$sum": bson.M{
						"$cond": bson.M{
							"if":   bson.M{"$eq": []interface{}{"$type", "INPUT"}},
							"then": "$cant",
							"else": 0,
						},
					},
				},
				"totalOutput": bson.M{
					"$sum": bson.M{
						"$cond": bson.M{
							"if":   bson.M{"$eq": []interface{}{"$type", "OUTPUT"}},
							"then": "$cant",
							"else": 0,
						},
					},
				},
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, err
	}
	for cursor.Next(database.Ctx) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	fmt.Println("y1")
	return &result, nil
}
