package controller

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"warehouse-project/database"
	"warehouse-project/graph/model"
)

const (
	CollectionCategory = "categories"
)

type ControllerCategory interface {
	Add(book *model.Category) (*model.Category, error)
	Update(id string, book *model.Category) *model.Category
	Remove(id string) bool
	GetById(id string) *model.Category
	GetAll() []*model.Category
}

func AddCategory(categoryInput model.InputCategory) (*model.Category, error) {
	c, errC := database.GetCollection(CollectionCategory)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	idCategory := primitive.NewObjectID()
	category := bson.M{
		"id":          idCategory,
		"name":        categoryInput.Name,
		"description": categoryInput.Description,
	}
	_, errC = c.InsertOne(database.Ctx, category)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	return GetCategory(idCategory.Hex())
}

func GetCategory(idCategory string) (*model.Category, error) {
	c, errC := database.GetCollection(CollectionCategory)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdCategory, _ := primitive.ObjectIDFromHex(idCategory)
	var result model.Category
	errC = c.FindOne(database.Ctx, bson.M{"id": objectIdCategory}).Decode(&result)
	return &result, errC
}

func UpdateCategory(idCategory string, category model.InputCategory) (*model.Category, error) {
	c, errC := database.GetCollection(CollectionCategory)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdCategory, _ := primitive.ObjectIDFromHex(idCategory)
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"id": objectIdCategory},
		bson.M{"$set": category},
	)
	if err != nil {
		return nil, fmt.Errorf("Error 500. Couldn't update category %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return nil, errors.New("Error 404. Holiday not found ")
	}
	return GetCategory(idCategory)
}

func RemoveCategory(idCategory string) (bool, error) {
	c, errC := database.GetCollection(CollectionCategory)
	if errC != nil {
		return false, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdCategory, _ := primitive.ObjectIDFromHex(idCategory)
	result, err := c.DeleteOne(database.Ctx, bson.M{"id": objectIdCategory})
	if err != nil {
		return false, fmt.Errorf("Error 500. Couldn't delete category %v ", err)
	}
	if result.DeletedCount == 0 {
		return false, errors.New("Error 404. Category not found ")
	}
	return true, nil
}

func GetCategories() ([]*model.Category, error) {
	c, errorC := database.GetCollection(CollectionCategory)
	if errorC != nil {
		return nil, errorC
	}
	var results []*model.Category
	cursor, err := c.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}
	err = cursor.All(context.TODO(), &results)
	return results, err
}


//
////NroCategorys is a function for search all MediaReports from items
//func NroCategorys(idCategory primitive.ObjectID) (int, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return 0, errC
//	}
//	var results struct {
//		Nro int `json:"nro" bson:"nro"`
//	}
//	benefits := bson.A{
//		bson.D{{
//			"$lookup", bson.M{
//				"from":         "benefits",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "nroCategorys",
//			},
//		}},
//		bson.D{{
//			"$project", bson.M{
//				"_id": "0",
//				"total": bson.M{
//					"$cond": bson.M{
//						"if":   bson.M{"$isArray": "$nroCategorys"},
//						"then": bson.M{"$size": "$nroCategorys"},
//						"else": 0,
//					},
//				},
//			},
//		}},
//	}
//
//	pendings := bson.A{
//		bson.D{{
//			"$lookup", bson.M{
//				"from":         "pendings",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "nroCategorys",
//			},
//		}},
//		bson.D{{
//			"$project", bson.M{
//				"_id": "0",
//				"total": bson.M{
//					"$cond": bson.M{
//						"if":   bson.M{"$isArray": "$nroCategorys"},
//						"then": bson.M{"$size": "$nroCategorys"},
//						"else": 0,
//					},
//				},
//			},
//		}},
//	}
//
//	disbursements := bson.A{
//		bson.D{{
//			"$lookup", bson.M{
//				"from":         "disbursements",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "nroCategorys",
//			},
//		}},
//		bson.D{{
//			"$project", bson.M{
//				"_id": "0",
//				"total": bson.M{
//					"$cond": bson.M{
//						"if":   bson.M{"$isArray": "$nroCategorys"},
//						"then": bson.M{"$size": "$nroCategorys"},
//						"else": 0,
//					},
//				},
//			},
//		}},
//	}
//
//	pipeline := bson.A{
//		bson.D{{"$match", bson.M{"_id": idCategory}}},
//		bson.D{{"$facet", bson.D{{"benefits", benefits}, {"pendings", pendings}, {"disbursements", disbursements}}}},
//		bson.D{{
//			"$project", bson.M{
//				"array": bson.M{
//					"$concatArrays": []interface{}{"$benefits", "$pendings", "$disbursements"},
//				},
//			},
//		}},
//		bson.D{{
//			"$project", bson.M{
//				"nro": bson.M{"$sum": "$array.total"},
//			},
//		}},
//	}
//	cursor, err := c.Aggregate(database.Ctx, pipeline)
//	if err != nil {
//		return 0, err
//	}
//	for cursor.Next(database.Ctx) {
//		if err = cursor.Decode(&results); err != nil {
//			return 0, err
//		}
//	}
//	return results.Nro, errC
//}
//
//func Pagination(
//	first, offset int,
//	idProvince *primitive.ObjectID,
//	hasProvince bool,
//	idDistrict *primitive.ObjectID,
//	hasDistrict bool,
//	locality string,
//	hasLocality bool,
//	gender string,
//	hasGender bool,
//) (*model.CategoryPageCursor, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, fmt.Errorf("Error 500. %v ", errC)
//	}
//	filter := bson.M{}
//	if hasProvince {
//		filter["idProvince"] = *idProvince
//	}
//	if hasDistrict {
//		filter["idDistrict"] = *idDistrict
//	}
//	if hasLocality {
//		filter["locality"] = locality
//	}
//	if hasGender {
//		filter["gender"] = gender
//	}
//
//	pipeline := []bson.M{
//		{
//			"$match": filter,
//		},
//	}
//	pipeline = append(pipeline, []bson.M{
//		{
//			"$group": bson.M{
//				"_id":   nil,
//				"nodes": bson.M{"$push": "$$ROOT"},
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":        1,
//				"nodes":      1,
//				"totalCount": bson.M{"$size": "$nodes"},
//			},
//		},
//		{
//			"$project": bson.M{
//				"totalCount": 1,
//				"nodes": bson.M{
//					"$slice": bson.A{"$nodes", offset, first},
//				},
//			},
//		},
//	}...)
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	var results model.CategoryPageCursor
//	for cursor.Next(database.Ctx) {
//		if err = cursor.Decode(&results); err != nil {
//			return nil, err
//		}
//	}
//	return &results, err
//}
//
//func GetNumberCategorysByGender() ([]model.NumberCategorysByFilter, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, fmt.Errorf("Error 500. %v ", errC)
//	}
//	pipeline := []bson.M{
//		{
//			"$group": bson.M{
//				"_id": "$gender",
//				"categorys": bson.M{
//					"$push": "$_id",
//				},
//			},
//		},
//		{
//			"$project": bson.M{
//				"gender": "$_id",
//				"total":  bson.M{"$size": "$categorys"},
//			},
//		},
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	var results []model.NumberCategorysByFilter
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//}
//
//func GetNumberCategorysByStatus() ([]model.NumberCategorysByFilter, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, fmt.Errorf("Error 500. %v ", errC)
//	}
//	pipeline := []bson.M{
//		{
//			"$group": bson.M{
//				"_id":   "$status",
//				"count": bson.M{"$sum": 1},
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":    0,
//				"status": "$_id",
//				"total":  "$count",
//			},
//		},
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	var results []model.NumberCategorysByFilter
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//}
//
//func GetNumberCategorysByFilter(filter string) ([]model.NumberCategorysByFilter, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, fmt.Errorf("Error 500. %v ", errC)
//	}
//	pipeline := []bson.M{
//		{
//			"$group": bson.M{
//				"_id":   "$" + filter,
//				"count": bson.M{"$sum": 1},
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":   0,
//				filter:  "$_id",
//				"total": "$count",
//			},
//		},
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	var results []model.NumberCategorysByFilter
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//}
//
//func GetReportsByCategoryLocality() ([]model.ReportsByCategory, error) {
//	c, errC := database.GetCollection(ColBenefit)
//	if errC != nil {
//		return nil, fmt.Errorf("Error 500. %v ", errC)
//	}
//	pipeline := []bson.M{
//		{
//			"$addFields": bson.M{
//				"endYear":   bson.M{"$year": "$end"},
//				"startYear": bson.M{"$year": "$start"},
//			},
//		},
//		{
//			"$project": bson.M{
//				"idCategory": 1,
//				"years": bson.M{
//					"$range": []interface{}{
//						bson.M{"$sum": "$startYear"},
//						bson.M{"$sum": []interface{}{"$endYear", 1}},
//						1,
//					},
//				},
//			},
//		},
//		{
//			"$unwind": "$years",
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "categorys",
//				"localField":   "idCategory",
//				"foreignField": "_id",
//				"as":           "category",
//			},
//		},
//		{
//			"$unwind": "$category",
//		},
//		{
//			"$project": bson.M{
//				"idCategory": 1,
//				"year":      "$years",
//				"locality":  "$category.locality",
//			},
//		},
//		{
//			"$group": bson.M{
//				"_id": bson.M{
//					"locality": "$locality",
//					"year":     "$year",
//				},
//				"categorys": bson.M{"$addToSet": "$idCategory"},
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":      0,
//				"total":    bson.M{"$size": "$categorys"},
//				"year":     "$_id.year",
//				"locality": "$_id.locality",
//			},
//		},
//		{
//			"$group": bson.M{
//				"_id":   "$year",
//				"years": bson.M{"$addToSet": "$$ROOT"},
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":   1,
//				"years": 1,
//			},
//		},
//		{
//			"$sort": bson.M{"_id": 1},
//		},
//	}
//	//fmt.Println(pipeline)
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	var results []model.ReportsByCategory
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//
//	return results, err
//}
//
//func GetReportsByCategoryStudent(typeReport string) ([]model.ReportsByCategory, error) {
//	collection, localField, result, foreignField := attrReport(typeReport)
//	c, errC := database.GetCollection(ColBenefit)
//	if errC != nil {
//		return nil, fmt.Errorf("Error 500. %v ", errC)
//	}
//	pipeline := []bson.M{
//		{
//			"$addFields": bson.M{
//				"endYear":   bson.M{"$year": "$end"},
//				"startYear": bson.M{"$year": "$start"},
//			},
//		},
//		{
//			"$project": bson.M{
//				"idCategory": 1,
//				"years": bson.M{
//					"$range": []interface{}{
//						bson.M{"$sum": "$startYear"},
//						bson.M{"$sum": []interface{}{"$endYear", 1}},
//						1,
//					},
//				},
//			},
//		},
//		{
//			"$unwind": "$years",
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "students",
//				"localField":   "idCategory",
//				"foreignField": "idCategory",
//				"as":           "student",
//			},
//		},
//		{
//			"$unwind": "$student",
//		},
//		{
//			"$lookup": bson.M{
//				"from":         collection,
//				"localField":   "student." + localField,
//				"foreignField": "_id",
//				"as":           result,
//			},
//		},
//		{
//			"$unwind": "$" + result,
//		},
//		{
//			"$project": bson.M{
//				"idCategory": 1,
//				"year":      "$years",
//				result:      "$" + result + "." + foreignField,
//			},
//		},
//		{
//			"$group": bson.M{
//				"_id": bson.M{
//					result: "$" + result,
//					"year": "$year",
//				},
//				"categorys": bson.M{"$addToSet": "$idCategory"},
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":   0,
//				"total": bson.M{"$size": "$categorys"},
//				"year":  "$_id.year",
//				result:  "$_id." + result,
//			},
//		},
//		{
//			"$group": bson.M{
//				"_id":   "$year",
//				"years": bson.M{"$addToSet": "$$ROOT"},
//			},
//		},
//		{
//			"$sort": bson.M{"_id": 1},
//		},
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	var results []model.ReportsByCategory
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//}
//
//func attrReport(typeReport string) (string, string, string, string) {
//	var (
//		collection   string
//		localField   string
//		result       string
//		foreignField string
//	)
//
//	switch typeReport {
//	case "career":
//		collection = "careers"
//		localField = "idCareer"
//		result = "career"
//		foreignField = "generalCareer"
//	case "region":
//		collection = "academicCenters"
//		localField = "idAcademicCenter"
//		result = "region"
//		foreignField = "city"
//	case "university":
//		collection = "academicCenters"
//		localField = "idAcademicCenter"
//		result = "university"
//		foreignField = "name"
//	}
//
//	return collection, localField, result, foreignField
//}
//
//func GetLocalities() ([]string, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, errC
//	}
//	var results struct {
//		Localities []string `json:"localities" bson:"localities"`
//	}
//	pipeline := []bson.M{
//		{
//			"$group": bson.M{
//				"_id": "$locality",
//			},
//		},
//		{
//			"$addFields": bson.M{
//				"locality": "locality",
//			},
//		},
//		{
//			"$group": bson.M{
//				"_id": "$locality",
//				"localities": bson.M{
//					"$push": "$_id",
//				},
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":        0,
//				"localities": "$localities",
//			},
//		},
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, err
//	}
//	for cursor.Next(database.Ctx) {
//		if err = cursor.Decode(&results); err != nil {
//			return nil, err
//		}
//	}
//	return results.Localities, nil
//}
//
//func GetCategoryAndAnnualBenefitByYear(year int) ([]model.Category, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, errC
//	}
//	var results []model.Category
//	pipeline := []bson.M{
//		{
//			"$lookup": bson.M{
//				"from":         "annualBenefits",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "annualBenefits",
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":               1,
//				"idUser":            1,
//				"bankAccount":       1,
//				"additionalCourses": 1,
//				"idProvince":        1,
//				"idDistrict":        1,
//				"locality":          1,
//				"gender":            1,
//				"status":            1,
//				"address":           1,
//				"annualBenefit": bson.M{
//					"$filter": bson.M{
//						"input": "$annualBenefits",
//						"as":    "annualBenefit",
//						"cond":  bson.M{"$eq": []interface{}{"$$annualBenefit.year", year}},
//					},
//				},
//			},
//		},
//		{
//			"$unwind": bson.M{
//				"path":                       "$annualBenefit",
//				"preserveNullAndEmptyArrays": true,
//			},
//		},
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//
//}
//
//func GetCategoryByLocality(locality string, year int) ([]model.CategoryByLocality, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, errC
//	}
//	var results []model.CategoryByLocality
//	pipeline := []bson.M{
//		{
//			"$match": bson.M{
//				"locality": locality,
//			},
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "users",
//				"localField":   "idUser",
//				"foreignField": "_id",
//				"as":           "user",
//			},
//		},
//		{
//			"$unwind": "$user",
//		},
//		{
//			"$project": bson.M{
//				"_id": 1,
//				"name": bson.M{
//					"$concat": []interface{}{"$user.names", " ", "$user.ln", " ", "$user.mln"},
//				},
//				"status": 1,
//			},
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "annualBenefits",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "annualBenefit",
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":    1,
//				"name":   1,
//				"status": 1,
//				"annualBenefit": bson.M{
//					"$filter": bson.M{
//						"input": "$annualBenefit",
//						"as":    "collection",
//						"cond":  bson.M{"$eq": []interface{}{"$$collection.year", year}},
//					},
//				},
//			},
//		},
//		{
//			"$unwind": bson.M{
//				"path":                       "$annualBenefit",
//				"preserveNullAndEmptyArrays": true,
//			},
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "disbursements",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "disbursements",
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":    1,
//				"name":   1,
//				"status": 1,
//				"disbursements": bson.M{
//					"$filter": bson.M{
//						"input": "$disbursements",
//						"as":    "collection",
//						"cond":  bson.M{"$eq": []interface{}{"$$collection.year", year}},
//					},
//				},
//				"annualBenefit": 1,
//			},
//		},
//		{
//			"$unwind": bson.M{
//				"path":                       "$disbursements",
//				"preserveNullAndEmptyArrays": true,
//			},
//		},
//		{
//			"$sort": bson.M{"disbursements.endMonth": 1},
//		},
//		{
//			"$group": bson.M{
//				"_id":           "$_id",
//				"status":        bson.M{"$first": "$status"},
//				"name":          bson.M{"$first": "$name"},
//				"disbursement":  bson.M{"$push": "$disbursements"},
//				"annualBenefit": bson.M{"$first": "$annualBenefit"},
//			},
//		},
//		{
//			"$project": bson.M{
//				"status": 1,
//				"name":   1,
//				"disbursement": bson.M{
//					"$last": "$disbursement",
//				},
//				"annualBenefit": 1,
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":              1,
//				"name":             1,
//				"status":           1,
//				"lastDisbursement": 1,
//				"jan": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.jan.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"feb": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.feb.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"mar": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.mar.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"apr": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.apr.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"may": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.may.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"jun": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.jun.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"jul": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.jul.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"aug": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.aug.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"sep": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.sep.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"oct": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.oct.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"nov": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.nov.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//				"dec": bson.M{
//					"$cond": bson.M{
//						"if": bson.M{
//							"$gt": []interface{}{"$annualBenefit.dec.totalExpenses", 0},
//						},
//						"then": true,
//						"else": false,
//					},
//				},
//			},
//		},
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//
//}
//
//func GetCategoryForXlsx(categoryType string) ([]model.CategoryXlsx, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, errC
//	}
//	var results []model.CategoryXlsx
//	var pipelineResult []bson.M
//	pipeline := []bson.M{
//		{
//			"$lookup": bson.M{
//				"from":         "users",
//				"localField":   "idUser",
//				"foreignField": "_id",
//				"as":           "user",
//			},
//		},
//		{
//			"$unwind": "$user",
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "provinces",
//				"localField":   "idProvince",
//				"foreignField": "_id",
//				"as":           "province",
//			},
//		},
//		{
//			"$unwind": "$province",
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "districts",
//				"localField":   "idDistrict",
//				"foreignField": "_id",
//				"as":           "district",
//			},
//		},
//		{
//			"$unwind": "$district",
//		},
//		{
//			"$project": bson.M{
//				"_id": 0,
//				"status": bson.M{
//					"$switch": bson.M{
//						"branches": []interface{}{
//							bson.M{"case": bson.M{"$eq": []interface{}{"CO", "$status"}}, "then": "Continua"},
//							bson.M{"case": bson.M{"$eq": []interface{}{"C", "$status"}}, "then": "Culmino"},
//							bson.M{"case": bson.M{"$eq": []interface{}{"A", "$status"}}, "then": "Abandono"},
//							bson.M{"case": bson.M{"$eq": []interface{}{"SR", "$status"}}, "then": "Sin Respuesta"},
//						},
//					},
//				},
//				"name":  bson.M{"$concat": []interface{}{"$user.names", " ", "$user.ln", " ", "$user.mln"}},
//				"phone": "$user.phone",
//				"email": "$user.email",
//				"nick":  "$user.nick",
//				"dni":   "$user.dni",
//				"gender": bson.M{
//					"$switch": bson.M{
//						"branches": []interface{}{
//							bson.M{"case": bson.M{"$eq": []interface{}{"F", "$gender"}}, "then": "Femenino"},
//							bson.M{"case": bson.M{"$eq": []interface{}{"M", "$gender"}}, "then": "Masculino"},
//						},
//					},
//				},
//				"bankAccount":   "$bankAccount",
//				"bankingEntity": "$bankingEntity",
//				"provinceName":  "$province.name",
//				"districtName":  "$district.name",
//				"locality":      "$locality",
//				"address":       "$address",
//			},
//		},
//	}
//	if categoryType != "T" {
//		pipelineResult = append(pipelineResult,
//			bson.M{
//				"$match": bson.M{"status": categoryType},
//			})
//		pipelineResult = append(pipelineResult, pipeline...)
//	} else {
//		pipelineResult = pipeline
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipelineResult)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//}
//
//func GetAnnualReportFinancial(year int) ([]model.AnnualReportFinancial, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, fmt.Errorf("Error 500. %v ", errC)
//	}
//	pipeline := []bson.M{
//		{
//			"$lookup": bson.M{
//				"from":         "users",
//				"localField":   "idUser",
//				"foreignField": "_id",
//				"as":           "user",
//			},
//		},
//		{
//			"$unwind": "$user",
//		},
//		{
//			"$project": bson.M{
//				"_id":    1,
//				"name":   "$user.names",
//				"ln":     "$user.ln",
//				"mln":    "$user.mln",
//				"status": 1,
//			},
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "annualBenefits",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "annualBenefit",
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":    1,
//				"name":   1,
//				"ln":     1,
//				"mln":    1,
//				"status": 1,
//				"annualBenefit": bson.M{
//					"$filter": bson.M{
//						"input": "$annualBenefit",
//						"as":    "collection",
//						"cond":  bson.M{"$eq": []interface{}{"$$collection.year", year}},
//					},
//				},
//			},
//		},
//		{
//			"$unwind": bson.M{
//				"path":                       "$annualBenefit",
//				"preserveNullAndEmptyArrays": true,
//			},
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "disbursements",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "disbursements",
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":    1,
//				"name":   1,
//				"status": 1,
//				"ln":     1,
//				"mln":    1,
//				"disbursements": bson.M{
//					"$filter": bson.M{
//						"input": "$disbursements",
//						"as":    "collection",
//						"cond":  bson.M{"$eq": []interface{}{"$$collection.year", year}},
//					},
//				},
//				"annualBenefit": 1,
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":    1,
//				"name":   1,
//				"status": 1,
//				"ln":     1,
//				"mln":    1,
//				"deposit": bson.M{
//					"$sum": "$disbursements.amount",
//				},
//				"totalToDeposit": bson.M{"$ifNull": []interface{}{"$annualBenefit.totalAmount", 0}},
//				"jan":            bson.M{"$ifNull": []interface{}{"$annualBenefit.jan.totalAmount", 0}},
//				"feb":            bson.M{"$ifNull": []interface{}{"$annualBenefit.feb.totalAmount", 0}},
//				"mar":            bson.M{"$ifNull": []interface{}{"$annualBenefit.mar.totalAmount", 0}},
//				"apr":            bson.M{"$ifNull": []interface{}{"$annualBenefit.apr.totalAmount", 0}},
//				"may":            bson.M{"$ifNull": []interface{}{"$annualBenefit.may.totalAmount", 0}},
//				"jun":            bson.M{"$ifNull": []interface{}{"$annualBenefit.jun.totalAmount", 0}},
//				"jul":            bson.M{"$ifNull": []interface{}{"$annualBenefit.jul.totalAmount", 0}},
//				"aug":            bson.M{"$ifNull": []interface{}{"$annualBenefit.aug.totalAmount", 0}},
//				"sep":            bson.M{"$ifNull": []interface{}{"$annualBenefit.sep.totalAmount", 0}},
//				"oct":            bson.M{"$ifNull": []interface{}{"$annualBenefit.oct.totalAmount", 0}},
//				"nov":            bson.M{"$ifNull": []interface{}{"$annualBenefit.nov.totalAmount", 0}},
//				"dec":            bson.M{"$ifNull": []interface{}{"$annualBenefit.dec.totalAmount", 0}},
//			},
//		},
//		{
//			"$addFields": bson.M{
//				"notDeposit": bson.M{
//					"$subtract": []interface{}{"$totalToDeposit", "$deposit"},
//				},
//			},
//		},
//	}
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	var results []model.AnnualReportFinancial
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//}
//
//func GetCategorysWithFilters(
//	academicCenter string,
//	hasAcademicCenter bool,
//	status string,
//	hasStatus bool,
//	cities []string,
//	hasCities bool,
//) ([]model.CategoryFilter, error) {
//	c, errC := database.GetCollection(CollectionCategory)
//	if errC != nil {
//		return nil, fmt.Errorf("Error 500. %v ", errC)
//	}
//	filter := bson.M{}
//	if hasAcademicCenter {
//		filter["academicCenter"] = academicCenter
//	}
//	if hasStatus {
//		filter["status"] = status
//	}
//	//pipeline := []bson.M{}
//	pipeline := []bson.M{
//		{
//			"$lookup": bson.M{
//				"from":         "users",
//				"localField":   "idUser",
//				"foreignField": "_id",
//				"as":           "user",
//			},
//		},
//		{
//			"$unwind": "$user",
//		},
//		{
//			"$project": bson.M{
//				"_id":    1,
//				"status": 1,
//				"gender": 1,
//				"phone":  "$user.phone",
//				"dni":    "$user.dni",
//				"names":  "$user.names",
//				"ln":     "$user.ln",
//				"mln":    "$user.mln",
//				"idUser": "$user._id",
//			},
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "students",
//				"localField":   "_id",
//				"foreignField": "idCategory",
//				"as":           "student",
//			},
//		},
//		{
//			"$unwind": bson.M{
//				"path":                       "$student",
//				"preserveNullAndEmptyArrays": true,
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":              1,
//				"status":           1,
//				"gender":           1,
//				"phone":            1,
//				"names":            1,
//				"ln":               1,
//				"mln":              1,
//				"idUser":           1,
//				"dni":              1,
//				"studyPlace":       bson.M{"$ifNull": []interface{}{"$student.studyPlace", ""}},
//				"idCareer":         bson.M{"$ifNull": []interface{}{"$student.idCareer", ""}},
//				"idAcademicCenter": bson.M{"$ifNull": []interface{}{"$student.idAcademicCenter", ""}},
//			},
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "academicCenters",
//				"localField":   "idAcademicCenter",
//				"foreignField": "_id",
//				"as":           "academicCenter",
//			},
//		},
//		{
//			"$unwind": bson.M{
//				"path":                       "$academicCenter",
//				"preserveNullAndEmptyArrays": true,
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":            1,
//				"status":         1,
//				"gender":         1,
//				"phone":          1,
//				"names":          1,
//				"ln":             1,
//				"mln":            1,
//				"studyPlace":     1,
//				"idUser":         1,
//				"dni":            1,
//				"academicCenter": bson.M{"$ifNull": []interface{}{"$academicCenter.name", ""}},
//				"idCareer":       1,
//			},
//		},
//		{
//			"$lookup": bson.M{
//				"from":         "careers",
//				"localField":   "idCareer",
//				"foreignField": "_id",
//				"as":           "career",
//			},
//		},
//		{
//			"$unwind": bson.M{
//				"path":                       "$career",
//				"preserveNullAndEmptyArrays": true,
//			},
//		},
//		{
//			"$project": bson.M{
//				"_id":            1,
//				"status":         1,
//				"gender":         1,
//				"phone":          1,
//				"names":          1,
//				"ln":             1,
//				"mln":            1,
//				"studyPlace":     1,
//				"idUser":         1,
//				"academicCenter": 1,
//				"dni":            1,
//				"career":         bson.M{"$ifNull": []interface{}{"$career.name", ""}},
//			},
//		},
//	}
//	pipeline = append(pipeline, bson.M{
//		"$match": filter,
//	})
//
//	if hasCities {
//		pipeline = append(pipeline, bson.M{
//			"$match": bson.M{
//				"studyPlace": bson.M{"$in": cities},
//			},
//		})
//	}
//
//	cursor, err := c.Aggregate(context.TODO(), pipeline)
//	if err != nil {
//		return nil, fmt.Errorf("Error 500. Couldn't find %v ", err)
//	}
//	var results []model.CategoryFilter
//	if err = cursor.All(context.TODO(), &results); err != nil {
//		return nil, err
//	}
//	return results, err
//}
