package controller

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"warehouse-project/database"
	"warehouse-project/graph/model"
)

const (
	CollectionArticle = "articles"
)

type ControllerArticle interface {
	Add(book *model.Article) (*model.Article, error)
	Update(id string, book *model.Article) *model.Article
	Remove(id string) bool
	GetById(id string) *model.Article
	GetAll() []*model.Article
}

func AddArticle(articleInput model.InputArticle) (*model.Article, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	idArticle := primitive.NewObjectID()
	objectIDWarehouse, _ := primitive.ObjectIDFromHex(articleInput.IDWarehouse)
	objectIDUnitMeasure, _ := primitive.ObjectIDFromHex(articleInput.IDUnitMeasure)
	objectIDCategory, _ := primitive.ObjectIDFromHex(articleInput.IDCategory)
	article := bson.M{
		"_id":           idArticle,
		"id":            idArticle,
		"idCategory":    objectIDCategory,
		"idWarehouse":   objectIDWarehouse,
		"idUnitMeasure": objectIDUnitMeasure,
		"description":   articleInput.Description,
		"purchasePrice": articleInput.PurchasePrice,
		"dealerPrice":   articleInput.DealerPrice,
		"sellingPrice":  articleInput.SellingPrice,
	}
	_, errC = c.InsertOne(database.Ctx, article)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	return GetArticle(idArticle.Hex())
}

func UpdateArticle(idArticle string, articleInput model.InputArticle) (*model.Article, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdArticle, _ := primitive.ObjectIDFromHex(idArticle)
	objectIDWarehouse, _ := primitive.ObjectIDFromHex(articleInput.IDWarehouse)
	objectIDUnitMeasure, _ := primitive.ObjectIDFromHex(articleInput.IDUnitMeasure)
	objectIDCategory, _ := primitive.ObjectIDFromHex(articleInput.IDCategory)
	article := bson.M{
		"id":            idArticle,
		"idCategory":    objectIDCategory,
		"idWarehouse":   objectIDWarehouse,
		"idUnitMeasure": objectIDUnitMeasure,
		"description":   articleInput.Description,
		"purchasePrice": articleInput.PurchasePrice,
		"dealerPrice":   articleInput.DealerPrice,
		"sellingPrice":  articleInput.SellingPrice,
	}
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"id": objectIdArticle},
		bson.M{"$set": article},
	)
	if err != nil {
		return nil, fmt.Errorf("Error 500. Couldn't update article %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return nil, errors.New("Error 404. Holiday not found ")
	}
	return GetArticle(idArticle)
}

func RemoveArticle(idArticle string) (bool, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return false, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdArticle, _ := primitive.ObjectIDFromHex(idArticle)
	result, err := c.DeleteOne(database.Ctx, bson.M{"id": objectIdArticle})
	if err != nil {
		return false, fmt.Errorf("Error 500. Couldn't delete article %v ", err)
	}
	if result.DeletedCount == 0 {
		return false, errors.New("Error 404. Article not found ")
	}
	return true, nil
}

func GetArticle(idArticle string) (*model.Article, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdArticle, _ := primitive.ObjectIDFromHex(idArticle)
	var result model.Article
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"_id": objectIdArticle,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "categories",
				"localField":   "idCategory",
				"foreignField": "id",
				"as":           "category",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$category",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "unitMeasures",
				"localField":   "idUnitMeasure",
				"foreignField": "id",
				"as":           "unitMeasure",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$unitMeasure",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "warehouses",
				"localField":   "idWarehouse",
				"foreignField": "id",
				"as":           "warehouse",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$warehouse",
				"preserveNullAndEmptyArrays": true,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	for cursor.Next(context.TODO()) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

func GetArticles() ([]*model.Article, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	var results []*model.Article
	pipeline := []bson.M{
		{
			"$lookup": bson.M{
				"from":         "categories",
				"localField":   "idCategory",
				"foreignField": "id",
				"as":           "category",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$category",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "unitMeasures",
				"localField":   "idUnitMeasure",
				"foreignField": "id",
				"as":           "unitMeasure",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$unitMeasure",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "warehouses",
				"localField":   "idWarehouse",
				"foreignField": "id",
				"as":           "warehouse",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$warehouse",
				"preserveNullAndEmptyArrays": true,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	err = cursor.All(context.TODO(), &results)
	if err != nil {
		return nil, err
	}
	return results, nil
}

func UpdateStockArticle(idArticle primitive.ObjectID, stock float64) error {
	fmt.Println("b0")
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return fmt.Errorf("Error 500. %v ", errC)
	}
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"id": idArticle},
		bson.M{"$set": bson.M{
			"stock": stock,
		}},
	)
	if err != nil {
		return fmt.Errorf("Error 500. Couldn't update bond %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return errors.New("Error 404. career not found ")
	}
	return nil
}
